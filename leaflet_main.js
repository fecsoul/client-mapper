const host_name_local_map = "http://127.0.0.1:3000";
const urlStrapi1 = 'https://strapi-back-fakj.onrender.com';
const urlStrapi2 = 'https://strapi-back-uue0.onrender.com';
const dayOfMonth = new Date().getDate();
// Cambio servidores dependiendo el día
const host_name_map = (dayOfMonth >= 0 && dayOfMonth <= 31) ? urlStrapi2 : urlStrapi1;

const params = new URLSearchParams(window.location.search);
const tokenParam = params.get('token');
const userEmailParam = params.get('email');     
const roomParam = params.get('room');     
const passwordRoomParam = params.get('roompass');     
const addressInfo = document.getElementById('addressInfoID');
const detailsLabel = document.getElementById('detailsLabel');

const lastTimeConection = document.getElementById('lastTimeConection');

const latitudLabel = document.getElementById('txtLat');
const longitudLabel = document.getElementById('txtLon');

let latitude2 = -31.3740000;
let longitude2 = -58.0260000;

let userIDClicked = -1
const mymap = L.map('map').setView([latitude2, longitude2], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: ''
}).addTo(mymap);

let markers = [];

function getRandomColor() {
    const colors = ['red', 'blue', 'green', 'orange', 'purple', 'darkred', 'cadetblue', 'darkgreen'];
    return colors[Math.floor(Math.random() * colors.length)];
}

function createIcon(color) {
    return L.divIcon({
        className: 'custom-icon',
        html: `<div style="color: ${color};"><i class="fas fa-user"></i></div>`,
        iconSize: [30, 30], 
        iconAnchor: [15, 30],
    });
}
let paths = {}; 

function add_marker(latitude, longitude, id, usuario) {
    let markerObj = markers.find(m => m.userID === id);

    if (!markerObj) {
        if (latitude !== "null") {
            const color = getRandomColor();
            const icon = createIcon(color);
            const newMarker = L.marker([latitude, longitude], { icon }).addTo(mymap);
            newMarker.bindPopup(usuario);

            const userPath = L.polyline([[latitude, longitude]], { color }).addTo(mymap);
            paths[id] = userPath;

            newMarker.on('click', function() {
                getAddressFromCoordinates(latitude, longitude).then(data => {
                    userIDClicked = id;
                    let { city, quarter, road } = data.address;
                    addressInfo.innerHTML = `<div style="color: ${color}; display: inline-block; margin-right: 5px;"><i class="fas fa-user"></i></div>`;
                    latitudLabel.textContent = latitude;
                    longitudLabel.textContent = longitude;
                    detailsLabel.textContent = `${city}, ${quarter}, ${road}`;
                    lastTimeConection.textContent = ` ${usuario}`;
                });   
            });

            markers.push({ userID: id, marker: newMarker });
        }
    } else {
        const existingMarker = markerObj.marker;

        if (existingMarker.getLatLng().lat !== latitude || existingMarker.getLatLng().lng !== longitude) {
            existingMarker.setLatLng([latitude, longitude]);
            existingMarker.bindPopup(usuario);

            const existingPath = paths[id];
            existingPath.addLatLng([latitude, longitude]);

            if (userIDClicked === id) {
                getAddressFromCoordinates(latitude, longitude).then(data => {
                    let { city, quarter, road } = data.address;
                    detailsLabel.textContent = `${city}, ${quarter}, ${road}`;
                    lastTimeConection.textContent = ` ${usuario}`;
                    latitudLabel.textContent = latitude;
                    longitudLabel.textContent = longitude;
                });
            }
        }
    }
}


async function obtenerUsuarios() {
    const response = await fetch(`${host_name_map}/api/users/`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${tokenParam}`,
            'Content-Type': 'application/json'
        }
    });
    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return await response.json();
}

async function obtenerPosicion() {
    try {
        let usuarios = await obtenerUsuarios();
        usuarios.forEach(async (usuario) => {
            const response = await fetch(`${host_name_map}/api/locations/?filters%5Buserid%5D=${usuario.id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${tokenParam}`,
                    'Content-Type': 'application/json'
                }
            });
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            const data = await response.json();
            const userLocation = data.data[0];
            if (userLocation && userLocation.attributes) {
                const { latitude, longitude, userid, detail } = userLocation.attributes;
                add_marker(latitude, longitude, userid, detail);
            }
        });
    } catch (error) {
        console.error('Error al obtener la posición:', error);
    }
}

setInterval(obtenerPosicion, 5000);

function getAddressFromCoordinates(lat, lon) {
    return fetch(`https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lon}`)
        .then(response => response.json())
        .then(data => data)
        .catch(error => console.error('Error en reverse geocoding:', error));
}
